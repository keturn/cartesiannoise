# Copyright 2022 Kevin Turner
# SPDX-License-Identifier: Apache-2.0

import secrets
import struct
from typing import Union

import farmhash
import numpy as np
import pymorton
import scipy


int_packing = struct.Struct("<i")


def morton_fill(position: np.ndarray, shape: Union[tuple, np.ndarray], seed: int = None) \
        -> np.ndarray:
    if seed is None:
        seed = secrets.randbits(32)
    a = np.empty(shape, np.uint32)

    # we've imported these optimized functions in pymorton and farmhash
    # and then make them slow because pymorton doesn't work on numpy data types
    # and farmhash doesn't work on ints

    buf = bytearray(4)
    with np.nditer(a, flags=['multi_index'], op_flags=['writeonly']) as it:
        for x in it:
            point = position + it.multi_index
            z_order = pymorton.interleave3(*point.tolist())
            int_packing.pack_into(buf, 0, z_order)
            x[...] = farmhash.FarmHash32WithSeed(buf, seed)

    # Convert unsigned int32 uniform distribution to normal distribution
    return scipy.stats.norm.ppf(a / (1 << 32), 0).astype(np.float16)

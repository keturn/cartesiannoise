from unittest import TestCase

import numpy as np
from numpy.testing import assert_array_equal

from cartesiannoise.noise import morton_fill


class Test(TestCase):
    origin = np.asarray((0, 0, 0))
    shape = (4, 64, 64)

    # Arbitrary seeds stored here so the tests are reproducible.
    seeds = [0x7a8995e7, 0xae9d7a4f]

    def test_morton_fill_produces_array_with_values(self):
        a = morton_fill(self.origin, self.shape, self.seeds[0])

        self.assertEqual(self.shape, a.shape)

        # rough checks to make sure the result is not all zeros or all positive
        self.assertGreater(a.max(), 0.01)
        self.assertLess(a.min(), -0.01)

    def test_reproducible_with_same_seed(self):
        a = morton_fill(self.origin, self.shape, self.seeds[0])
        b = morton_fill(self.origin, self.shape, self.seeds[0])

        assert_array_equal(a, b)

    def test_different_seeds_give_different_results(self):
        a = morton_fill(self.origin, self.shape, self.seeds[0])
        b = morton_fill(self.origin, self.shape, self.seeds[1])

        self.assertNotEqual(a[0, 0].tolist(), b[0, 0].tolist())

    def test_overlapping_areas_are_consistent(self):
        shape_a = (4, 64, 64)
        shape_b = (1, 32, 32)

        overlap_x = 16
        overlap_y = 16

        origin_a = np.asarray((0, 0, 0))
        origin_b = np.asarray(
            (1, origin_a[1] + shape_a[1] - overlap_x, origin_a[2] - overlap_y))

        # That's a mess of arithmetic, but it should look like this:
        #
        #      bb
        #   aaaXb
        #   aaaa
        #   aaaa
        #   aaaa

        a = morton_fill(origin_a, shape_a, self.seeds[0])
        b = morton_fill(origin_b, shape_b, self.seeds[0])

        x_min = origin_b[1]
        y_min = origin_a[0]
        assert_array_equal(
            a[origin_b[0], x_min:x_min + overlap_x, y_min:y_min + overlap_y],
            b[0, 0:overlap_x, (-overlap_y):],
        )
